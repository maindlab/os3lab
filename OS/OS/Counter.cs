using System;
using System.Collections.Generic;
using System.Threading;

namespace OS
{
    public class Counter
    {
        private readonly List<char> _list;
        private readonly Mutex _mutex;
        private readonly int _k;

        public Counter(List<char> list, Mutex mutex, int k)
        {
            _list = list;
            _mutex = mutex;
            _k = k;
        }

        private void Count()
        {
            _mutex.WaitOne();
            var count = 0;
            for (int i = 0; i < _k; i++)
            {
                count += _list[i];
            }

            Console.WriteLine($"result for count is:{count}");
            _mutex.ReleaseMutex();
        }

        public void Start()
        {
            new Thread(Count).Start();
        }
    }
}
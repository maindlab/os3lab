﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OS
{
    class Program
    {
        static void Main(string[] args)
        {
            int arraySize;
            int k;//count after start third thread
            int timeToRelax;//after each element put in array
           // Console.WriteLine("");
           var c = 0;
           List<int> list = new List<int>();
           for (int i = 0; i < 100000000; i++)
           {
               list.Add(i);
           }
           Stopwatch sw= new Stopwatch();
           sw.Start();
           foreach (var l in list)
           {
               if (1 == l) c++;
           }
           sw.Stop();
           Console.WriteLine(sw.ElapsedMilliseconds);
           c = 0;
           sw.Restart();
           Parallel.ForEach(list, x =>
           {
               if (1 == x) c++;
           });
           sw.Stop();
           Console.WriteLine(sw.ElapsedMilliseconds);
           Console.WriteLine(c);
        }
    }
}
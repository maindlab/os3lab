using System.Threading;

namespace OS3
{
    public class StatMutex
    {
        private readonly Mutex _mutex = new Mutex();
        public bool IsChecked { get; set; }
        public bool IsSortEnd { get; set; } = false;
        public void WaitOne()
        {
            _mutex.WaitOne();
        }

        public void ReleaseMutex()
        {
            _mutex.ReleaseMutex();
        }
    }
}
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace OS3
{
    public class Sort
    {
        private IList<string> _list;
        private int _timeout;
        private StatMutex _mutex;
        private int _currentPosition=0;
        public Sort(IList<string> list, int timeout,StatMutex mutex)
        {
            _list = list;
            _timeout = timeout;
            _mutex = mutex;
        }

        public void Start()
        {
            var list = new List<string>(_list);
            foreach (var elem in list)
            {
                SortElem(elem);
            }

            _mutex.IsSortEnd = true;
        }

        private void SortElem(string elem)
        {
            _mutex.WaitOne();
            if (!_mutex.IsChecked)
            {
                _mutex.ReleaseMutex();
                return;
            }

            var count = 0;
            for (var i = _currentPosition; i < _list.Count; i++)
            {
                if (elem == _list[i]) count++;
            }

            if (count != 1)
            {
                _mutex.ReleaseMutex();
                return;
            }
            _currentPosition++;
            _list.Remove(elem);
            _list.Insert(0,elem);
            Thread.Sleep(_timeout);
            _mutex.IsChecked = false;
           _mutex.ReleaseMutex();
        }
    }
    
}
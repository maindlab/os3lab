﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;

namespace OS3
{
    class Program
    {
        private static int c=0;
        static void Main(string[] args)
        {
            Console.WriteLine("input k");
            var k = int.Parse(Console.ReadLine());
            var list = new List<string>();
            for (int i = 0; i < k; i++)
            {
                Console.WriteLine($"input {i} element:");
                list.Add(Console.ReadLine());
            }

            Console.Clear();
            Console.WriteLine($"array size {list.Count}\n" +
                              $"list contain {String.Join(", ",list)}");
            Console.WriteLine("input interval in ms");
            var interval = int.Parse(Console.ReadLine());
            Console.WriteLine("input numb for sum");
            var sumElm = int.Parse(Console.ReadLine());
            Console.Clear();
            var mutex = new StatMutex();
            mutex.IsChecked = true;
            var sumMutex = new Mutex();
            sumMutex.WaitOne();
            var sort  = new Sort(list,interval,mutex);
            
            
            new Thread(()=>Sum(sumMutex,list)).Start();
            new Thread(sort.Start).Start();
            while (!mutex.IsSortEnd)
            {
                Printer(mutex,list);
                if(c==sumElm) sumMutex.ReleaseMutex();
            }
            if(c<sumElm) sumMutex.ReleaseMutex();
            
        }

        private static void Printer(StatMutex mutex, IList<string> list)
        {
            mutex.WaitOne();
            if (mutex.IsChecked)
            {
                mutex.ReleaseMutex();
                return;
            }
            Console.WriteLine($"array size {list.Count}\n" +
                              $"list contain {String.Join(", ",list)}");
            mutex.IsChecked = true;
            c++;
            mutex.ReleaseMutex();
            
        }

        private static void Sum(Mutex mutex, List<string> list)
        {
            mutex.WaitOne();
            Console.WriteLine($"sum for string is {string.Join(String.Empty,list.GetRange(0,c))}");
            mutex.ReleaseMutex();
        }
    }
}
